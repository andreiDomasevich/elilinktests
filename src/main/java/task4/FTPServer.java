package task4;

import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.awt.List;
import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.net.*;

public class FTPServer {
    private static Socket socket1 = null;
    private static Socket socket = null;
    private static BufferedReader reader = null;
    private static BufferedReader reader1 = null;
    private static BufferedWriter writer = null;
    private static final Logger log = Logger.getLogger(task4.FTPServer.class);

    public void connection() throws IOException {
        log.info("Task 4 begin");
        log.info("connection to FTP ftp.byfly.by");
        socket = new Socket("ftp.byfly.by", 21); // подключаемся к ftp серверу
        reader = new BufferedReader(new InputStreamReader(socket.getInputStream())); // создаём канал ответов
        writer = new BufferedWriter(
                new OutputStreamWriter(socket.getOutputStream())); // создаём канал запросов

        System.out.println(reader.readLine()); // получаем ответ
        log.info("login and password");
        writer.write("USER anonymous" + "\r\n"); // вход на ftp сервер анонимно
        writer.flush();
        System.out.println(reader.readLine()); // считываем и выводим ответ в конслоль
        writer.write("PASS 123@google.com" + "\r\n");// вводим пароль для входа
        writer.flush();
        String line;


        line = reader.readLine(); // считываем ответ от сервера
        System.out.println(line); // вывод в консоль

        writer.write("PASV" + "\r\n"); // отправляем команду на вход в пассивыный режим
        writer.flush();

        line = reader.readLine(); // получаем ответ, в котором ip - адресс и порт, через которые будем получать ответ
        System.out.println(line); // вывод на экран
        String[] add = line.substring(line.indexOf('(')+1, line.indexOf(')')).split(","); //парсим ответ для получения ip адресса и порта
        int port = Integer.parseInt(add[4])*256 + Integer.parseInt(add[5]); // получаем порт, к которому надо подключиться
        String urll = add[0]+ "." + add[1] +"."+ add[2]+ "."+ add[3]; // получаем ip адресс
        socket1 = new Socket(InetAddress.getByName(urll), port); // подключаемся к сокету для получения данных
        reader1 = new BufferedReader(new InputStreamReader(socket1.getInputStream())); //создаем канал для чтения с нового сокета
        writer.write("RETR" + "\r\n"); // отправляем команду, что готовы считывать
        writer.flush();
        writer.write("LIST" + "\r\n"); // отправляем команду для получения списка файлов и деректорий в текущей дериктории
        writer.flush();
        ArrayList<String> list = new ArrayList<String>(); // список для запоминания дерикторий
        while((line = reader1.readLine())!=null)
        {

            if(line.charAt(0) == 'd') // если первый символ строки 'd' значит запоминаем название директории
            {
                String temp = line.substring((line.lastIndexOf(' ') + 1), line.length()); // получаем имя дериктории
                list.add(temp); // запоминаем его
                System.out.println(temp); //вывод на консоль
            }
        }
        System.out.println(reader.readLine()); // получаем ответ
        System.out.println(reader.readLine()); // получаем ответ
        for(String temp : list )
        {
            String command = "CWD /"+ temp + "\r\n"; // создаем команду, чтобы зайти в новую директорию
            System.out.println(command); // выводим эту команду
            writer.write(command); // отправляем эту команду
            writer.flush();
            reader.readLine(); // получаем ответ
            reader.readLine(); // получаем ответ
            reader1.close(); // закрываем канал чтения
            socket1.close(); // закрываем сокет

            writer.write("PASV" + "\r\n"); // отправляем команду на вход в пассивыный режим
            writer.flush();

            line = reader.readLine(); // получаем ответ, в котором ip - адресс и порт, через которые будем получать ответ
            System.out.println(line); // вывод на экран
            add = line.substring(line.indexOf('(')+1, line.indexOf(')')).split(","); //парсим ответ для получения ip адресса и порта
            port = Integer.parseInt(add[4])*256 + Integer.parseInt(add[5]); // получаем порт, к которому надо подключиться
            urll = add[0]+ "." + add[1] +"."+ add[2]+ "."+ add[3]; // получаем ip адресс
            socket1 = new Socket(InetAddress.getByName(urll), port); // подключаемся к сокету для получения данных
            reader1 = new BufferedReader(new InputStreamReader(socket1.getInputStream())); //создаем канал для чтения с нового сокета
            writer.write("RETR" + "\r\n"); // отправляем команду, что готовы считывать
            writer.flush();
            writer.write("LIST" + "\r\n"); // отправляем команду для получения списка файлов и деректорий в текущей дериктории
            writer.flush();


            // выводим список файлов и дерикторий в текущей дериктории
            while((line = reader1.readLine())!=null)
            {
                System.out.println(line);
            }


            writer.write("CWD /" + "\r\n"); // отправляем команду на возврат в корневую диекторию
            writer.flush();
            reader.readLine(); // получаем ответ
            reader.readLine(); // получаем ответ
            reader.readLine(); // получаем ответ

        }
        writer.write("MKD Directory" + "\r\n"); // отправляем команду на создание директории с именем Directory
        writer.flush();
        System.out.println(reader.readLine()); // получаем ответ
        line = reader.readLine(); // получаем ответ
        if(line.contains("550")) {
            System.out.println("Не удалось создать директорию");
            log.error("Не удалось создать директорию");
        } //если папка не создана выводим сообщение
        else
        {
            writer.write("RMD Directory" + "\r\n"); // отправляем команду на удаление созданной директории
            writer.flush();
        }


        socket.close(); // закрываем сокет
        socket1.close(); // закрываем сокет
        reader1.close(); // закрываем канал ответов
        reader.close(); // закрываем канал ответов
        writer.close(); // закрываем канал запросов

    }

}
