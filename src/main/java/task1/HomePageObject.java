package task1;

import helper.BasePage;
import helper.MyProperties;
import org.apache.log4j.Logger;
import org.openqa.selenium.*;

import java.util.ArrayList;
import java.util.List;

import static helper.SingletonWebDriver.getDriver;


public class HomePageObject extends BasePage {

    private static final Logger log = Logger.getLogger(HomePageObject.class);
    private static final By findButton = By.name("search");
    private static final By findField = By.id("search_from_str");
    private static final By resultsList = By.className("b-results__li");
    private static final By element = By.xpath("//*[.='Minsk Automated Testing Community']");
    private static List<WebElement> results = new ArrayList<>();
    private static WebElement minskAutTestCommunity;
    private final WebDriver driver = getDriver();
    protected static final String BASE_URL_1 = MyProperties.getMyPropertyToTask_1("URL");

    public void goToAutomatedTesting(){
        log.info("Task 1 begin");
        driver.get(BASE_URL_1);
        findElement(findField).sendKeys("automated testing");
        findElement(findButton).click();
        log.info("Searching for \"automated testing\"");

        results = getDriver().findElements(resultsList);
        System.out.println(results.size());
        log.info("Received " + results.size() + " elements");

        try {
            minskAutTestCommunity = findElement(element);
            minskAutTestCommunity.click();
            log.info("Trying go to find \"Minsk Automated Testing Community\"");
        } catch(WebDriverException e) {
            log.error("Element not found");
        }


    }
}
