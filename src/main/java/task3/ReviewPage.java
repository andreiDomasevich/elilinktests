package task3;

import helper.BasePage;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;

public class ReviewPage extends BasePage {
    private static final Logger log = Logger.getLogger(task3.PassengerInfoPageObject.class);
    private static final By completeButton = By.xpath(".//*[@id='continue_button']");

    public Boolean finish(){
        log.info("Find \"continue\" button");
        return findElement(completeButton).isEnabled();
    }

}
