package task3;

import helper.BasePage;
import helper.MyProperties;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;

import java.util.concurrent.TimeUnit;

import static helper.SingletonWebDriver.getDriver;

public class HomePageObject extends BasePage{

    private static final Logger log = Logger.getLogger(task3.HomePageObject.class);
    private static final WebDriver driver = getDriver();
    protected static final String BASE_URL_3 = MyProperties.getMyPropertyToTask_3("URL");
    private static final By fromLabel = By.xpath(".//*[@id='originCity']");
    private static final By toLabel = By.xpath(".//*[@id='destinationCity']");
    private static final By departureDate = By.xpath(".//*[@id='departureDate']");
    private static final By returnDate = By.xpath(".//*[@id='returnDate']");
    private static final By findButton = By.xpath(".//*[@id='findFlightsSubmit']");
    private static final By closeButton = By.xpath("html/body/div[31]/div[1]/button");//локатор лучше не придумал

    public void goToDelta(){
        log.info("Task 3 begin");
        driver.get(BASE_URL_3);
        try{
            findElement(closeButton).click();
        }catch(WebDriverException e){}
    }

    public void booking(){
        log.info("Booking");
        findElement(fromLabel).clear();
        findElement(fromLabel).sendKeys("JFK");
        findElement(fromLabel).sendKeys(Keys.ENTER);
        findElement(toLabel).sendKeys("SVO");
        findElement(toLabel).sendKeys(Keys.ENTER);
        findElement(departureDate).sendKeys("09/29/2017");
        findElement(returnDate).sendKeys("10/05/2017");
        findElement(findButton).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }


}
