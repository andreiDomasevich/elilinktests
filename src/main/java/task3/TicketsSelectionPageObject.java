package task3;

import org.apache.log4j.Logger;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.*;

import java.util.Random;
import static helper.SingletonWebDriver.getDriver;

public class TicketsSelectionPageObject {
    private static final Logger log = Logger.getLogger(task3.HomePageObject.class);
    private static final WebDriver driver = getDriver();
    private static final By departureTikets = By.xpath(".//*[@class='fareRowContainer']/td[2]");
    private static final By returnTikets = By.xpath(".//*[@class='fareRowContainer']/td[2]");
    private static final Random random = new Random();

    public void ticketsChoice(){
        log.info("choice tickets");
        int departureBound = driver.findElements(departureTikets).size();

        driver.findElements(departureTikets).get(random.nextInt(departureBound)).click();
        clickOn(returnTikets);

    }

    public void clickOn(By locator) {
        final WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.refreshed( ExpectedConditions.elementToBeClickable(locator)));
        int returnBound = driver.findElements(locator).size();
        driver.findElements(locator).get(random.nextInt(returnBound)).click();
    }

}
