package task3;

import helper.BasePage;
import helper.MyProperties;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;
import static helper.SingletonWebDriver.getDriver;

public class PassengerInfoPageObject extends BasePage {
    private static final Logger log = Logger.getLogger(task3.PassengerInfoPageObject.class);
    private static final WebDriver driver = getDriver();
    private static final By firstNameField  = By.xpath(".//*[@id='firstName0']");
    private static final By lastNameField  = By.xpath(".//*[@id='lastName0']");
    private static final By genderField  = By.xpath(".//*[@id='gender0-button']");
    private static final By monthField  = By.xpath(".//*[@id='month0-button']");
    private static final By monthMenu  = By.xpath(".//*[@id='month0-menu']/li");
    private static final By dayField  = By.xpath(".//*[@id='day0-button']");
    private static final By dayMenu  = By.xpath(".//*[@id='day0-menu']/li");
    private static final By yearField  = By.xpath(".//*[@id='year0-button']");
    private static final By yearMenu  = By.xpath(".//*[@id='year0-menu']/li");
    private static final By noButton  = By.xpath(".//*[@class='emergencyButtonWrapper buttonWrapper']/label[2]");
    private static final By deviceField  = By.xpath(".//*[@id='deviceType-button']");
    private static final By countryField  = By.xpath(".//*[@id='countryCode0-button']");
    private static final By countryMenu  = By.xpath(".//*[@id='countryCode0-menu']/li");
    private static final By phoneNumberField  = By.xpath(".//*[@id='telephoneNumber0']");
    private static final By emailField  = By.xpath(".//*[@id='email']");
    private static final By confirmEmailField  = By.xpath(".//*[@id='reEmail']");
    private static final By continueButton  = By.xpath(".//*[@id='paxReviewPurchaseBtn']");
    private static final String firstName = MyProperties.getMyPropertyToTask_3("firstName");
    private static final String lastName = MyProperties.getMyPropertyToTask_3("lastName");
    private static final String month = MyProperties.getMyPropertyToTask_3("month");
    private static final String day = MyProperties.getMyPropertyToTask_3("day");
    private static final String year = MyProperties.getMyPropertyToTask_3("year");
    private static final String telephoneNumber = MyProperties.getMyPropertyToTask_3("telephoneNumber");
    private static final String country = MyProperties.getMyPropertyToTask_3("country");
    private static final String email = MyProperties.getMyPropertyToTask_3("Email");
    static List<WebElement> birthList;

    public void fillFields() {
        log.info("First name and Last name");
        findElement(firstNameField).sendKeys(firstName);
        findElement(lastNameField).sendKeys(lastName);

        findElement(genderField).click();
        findElement(genderField).sendKeys(Keys.ARROW_DOWN);
        findElement(genderField).sendKeys(Keys.ENTER);

        log.info("Birthday");
        fill(monthField, monthMenu, month);
        fill(dayField, dayMenu, day);
        fill(yearField, yearMenu, year);

        findElement(noButton).click();
        findElement(phoneNumberField).sendKeys(telephoneNumber);

        log.info("Other information");
        findElement(deviceField).click();
        findElement(deviceField).sendKeys(Keys.ARROW_DOWN);
        findElement(deviceField).sendKeys(Keys.ENTER);
        fill(countryField, countryMenu, country);
        findElement(emailField).sendKeys(email);
        findElement(confirmEmailField).sendKeys(email);
        findElement(continueButton).click();
    }

    public void fill(By loc1, By loc2, String str){
        findElement(loc1).click();
        birthList = driver.findElements(loc2);
        for(int i = 0; i < birthList.size(); i ++)
        {
            if(birthList.get(i).getText().contains(str))
            {
                birthList.get(i).click();
                break;
            }
        }

    }
}