package task3;

import helper.BasePage;
import org.openqa.selenium.By;

public class SummaryPageObject extends BasePage{

    private final By continueButton = By.xpath(".//*[@id='tripSummarySubmitBtn']");

    public void clickContinue(){
        findElement(continueButton).click();
    }
}
