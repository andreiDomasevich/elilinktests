package task2;

import helper.BasePage;
import helper.MyProperties;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static helper.SingletonWebDriver.getDriver;

public class HomePageObject extends BasePage {

    private static final Logger log = Logger.getLogger(task2.HomePageObject.class);
    private static final WebDriver driver = getDriver();
    protected static final String BASE_URL_2 = MyProperties.getMyPropertyToTask_2("URL");
    protected static final String email = MyProperties.getMyPropertyToTask_2("email");
    protected static final String messageText = MyProperties.getMyPropertyToTask_2("messageText");
    protected static final String spam = MyProperties.getMyPropertyToTask_2("spam");
    private static final By loginLabel = By.id("identifierId");
    private static final By passwordLabel = By.name("password");
    protected static final String login = MyProperties.getMyPropertyToTask_2("login");
    protected static final String password = MyProperties.getMyPropertyToTask_2("password");
    private static final By sent = By.xpath("//*[.='Отправленные']");
    private static final By menu = By.xpath("//*[@id=\":4a\"]/div/div[2]");
    private static final By writeButton = By.xpath("//*[.='НАПИСАТЬ']");
    private static final By adresatLabel = By.name("to");
    private static final By messageField = By.xpath(".//*[@id='undefined']//div[2]/div");
    private static final String keysPressed =  Keys.chord(Keys.CONTROL, Keys.RETURN);
    private static final By logOutMenuButton = By.xpath(".//*[@class='gb_7a gbii']");
    private static final By logOutButton = By.xpath(".//*[@id='gb_71']");
    private static final By searchField = By.xpath(".//*[@id='gbqfq']");
    private static final By searchResult = By.xpath(".//*[@class='av']");
    private static final By briefs = By.xpath(".//*[@class='Di']/div[1]/span/span[1]/span[2]");
    private static final By oneBrief = By.xpath(".//*[@class='y6'][contains(.,'Shari')]");

    public void goToGmail(){
        log.info("Task 2 begin");
        driver.get(BASE_URL_2);
        log.info("LogIn");
        findElement(loginLabel).sendKeys(login);
        findElement(loginLabel).sendKeys(Keys.ENTER);
        findElement(passwordLabel).sendKeys(password);
        findElement(passwordLabel).sendKeys(Keys.ENTER);
    }

    public String goToInbox(){
        log.info("Inbox");
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        findElement(By.className("wT")).findElement(By.tagName("a")).click();
        return findElement(By.className("wT")).findElement(By.tagName("a")).getText();
    }

    public String goToSent(){
        log.info("Sent");
        Actions action = new Actions(driver);
        WebElement elem = driver.findElement(menu);
        action.moveToElement(elem);
        action.perform();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        findElement(sent).click();
        return driver.findElement(sent).getText();
    }

    public String goToSpam(){
        log.info("Spam");
        driver.get(spam);
        return driver.getCurrentUrl();
    }

    public void writeMessage() {
        log.info("write message");
        findElement(writeButton).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        findElement(adresatLabel).sendKeys(email);
        findElement(adresatLabel).sendKeys(Keys.ENTER);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        findElement(messageField).click();
        findElement(messageField).sendKeys(messageText);
        findElement(messageField).sendKeys();
        findElement(messageField).sendKeys(keysPressed) ;
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    public Boolean searchingInbox() {
        log.info("searching");
        findElement(searchField).sendKeys("shari");
        findElement(searchField).sendKeys(Keys.ENTER);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        List<WebElement> elements = driver.findElements(searchResult);
        List<WebElement> actualElements = driver.findElements(briefs);
        int flag = 0;
        if(Integer.parseInt(actualElements.get(2).getText()) == elements.size()) flag = 1;
        infoToList();
        if(flag == 1) return true;
        else return false;
    }

    public void infoToList() {
        log.info("get information");
        List<WebElement> briefsList = driver.findElements(oneBrief);
        ArrayList<ArrayList<String>> listOfLists = new ArrayList<ArrayList<String>>(briefsList.size());
        ArrayList<String> list = new ArrayList<String>();

        for(int i = 0; i < briefsList.size(); i++)
        {
            list.clear();
            list.add("INBOX");
            list.add(briefsList.get(i).getText());
            listOfLists.add(list);
        }

        System.out.println(listOfLists);
    }

    public void logOut() {
        log.info("LogOut");
        findElement(logOutMenuButton).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        findElement(logOutButton).click();
    }
}
