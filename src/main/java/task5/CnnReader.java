package task5;

import org.apache.log4j.Logger;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CnnReader {
    private static final Logger log = Logger.getLogger(task3.HomePageObject.class);

    static byte[] getUrlBytes(String urlSpec) throws IOException {
        log.info("Task 5 begin");
        log.info("connection and parsing");
        URL url = new URL(urlSpec);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            InputStream in = connection.getInputStream();
            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                return null;
            }
            int bytesRead = 0;
            byte[] buffer = new byte[1024];
            while ((bytesRead = in.read(buffer)) > 0) {
                out.write(buffer, 0, bytesRead);
            }
            out.close();
            return out.toByteArray();
        } finally {
            connection.disconnect();
        }
    }
    public static String getUrl(String urlSpec) throws IOException {
        return new String(getUrlBytes(urlSpec));
    }

    public static int countTrumps(String url) throws IOException {
        log.info("count results (using regular experrsions)");
        int count = 0;
        String response = CnnReader.getUrl(url);
        response.replaceAll("<script>(.*)</script>", "");

        Pattern p = Pattern.compile("\\bTrump\\b", Pattern.UNICODE_CASE);
        Matcher m = p.matcher(response);
        while(m.find()) {
            count++;
        }

        System.out.println(count);

        return count;
    }
}
