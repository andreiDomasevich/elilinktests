package helper;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

public class SingletonWebDriver {

    private static WebDriver driver;

    public SingletonWebDriver () {}

    public static WebDriver getDriver() {

        if (driver != null) {
            return driver;
        } else {
            return new ChromeDriver();
        }
    }

    public static WebDriver initDriver(String browser) throws MalformedURLException {

            switch (browser) {
                case "chrome": {
                    driver = new ChromeDriver();
                    break;
                }
                case "firefox": {
                    driver = new FirefoxDriver();
                    break;
                }
                default: {
                    driver = new FirefoxDriver();
                }
            }
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        return driver;
    }
}
