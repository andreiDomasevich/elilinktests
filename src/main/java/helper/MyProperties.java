package helper;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class MyProperties {

    private static final Properties PROJECT_PROPERTIES_1;
    private static final Properties PROJECT_PROPERTIES_2;
    private static final Properties PROJECT_PROPERTIES_3;
    private static final Properties PROJECT_PROPERTIES;

    static {
        PROJECT_PROPERTIES_1 = new Properties();
        PROJECT_PROPERTIES_2 = new Properties();
        PROJECT_PROPERTIES_3 = new Properties();
        PROJECT_PROPERTIES = new Properties();
        try {

            InputStream inputStream = MyProperties.class.getResourceAsStream("/properties.properties");
            PROJECT_PROPERTIES.load(inputStream);
            InputStream inputStream1 = MyProperties.class.getResourceAsStream("/task_1.properties");
            PROJECT_PROPERTIES_1.load(inputStream1);
            InputStream inputStream2 = MyProperties.class.getResourceAsStream("/task_2.properties");
            PROJECT_PROPERTIES_2.load(inputStream2);
            InputStream inputStream3 = MyProperties.class.getResourceAsStream("/task_3.properties");
            PROJECT_PROPERTIES_3.load(inputStream3);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String getMyProperty(String key) {
        String myProperty = PROJECT_PROPERTIES.getProperty(key);
        return myProperty;
    }

    public static String getMyPropertyToTask_1(String key) {
        String myProperty = PROJECT_PROPERTIES_1.getProperty(key);
        return myProperty;
    }

    public static String getMyPropertyToTask_2(String key) {
        String myProperty = PROJECT_PROPERTIES_2.getProperty(key);
        return myProperty;
    }

    public static String getMyPropertyToTask_3(String key) {
        String myProperty = PROJECT_PROPERTIES_3.getProperty(key);
        return myProperty;
    }
}
