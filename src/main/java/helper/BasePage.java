package helper;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class BasePage {

    public WebDriver driver = SingletonWebDriver.getDriver();

    public  WebElement findElement(By locator){

        return driver.findElement(locator);
    }
}
