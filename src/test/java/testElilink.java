import helper.TestBase;
import org.testng.annotations.Test;
import task4.FTPServer;
import task5.CnnReader;

import java.io.IOException;

import static org.testng.Assert.assertTrue;

public class testElilink extends TestBase {

    task1.HomePageObject homePageObjectTask1;
    task2.HomePageObject homePageObjectTask2;
    task3.HomePageObject homePageObjectTask3;
    task3.TicketsSelectionPageObject ticketsSelectionPageObject;
    task3.SummaryPageObject summaryPageObject;
    task3.PassengerInfoPageObject passengerInfoPageObject;
    task3.ReviewPage reviewPage;
    FTPServer ftpServer;

    @Test
    public void tutBy() {
        homePageObjectTask1 = new task1.HomePageObject();
        homePageObjectTask1.goToAutomatedTesting();
    }

    @Test(priority = 1)
    public void gMail() {
        homePageObjectTask2 = new task2.HomePageObject();
        homePageObjectTask2.goToGmail();
        assertTrue(homePageObjectTask2.goToSent().contains("Отправленные") || homePageObjectTask2.goToSent().contains("Sent"));
        assertTrue(homePageObjectTask2.goToSpam().contains("spam"));
        assertTrue(homePageObjectTask2.goToInbox().contains("Входящие") || homePageObjectTask2.goToInbox().contains("Inbox"));
        assertTrue(homePageObjectTask2.searchingInbox());
        homePageObjectTask2.writeMessage();
        homePageObjectTask2.logOut();
    }

    @Test(priority = 2)
    public void delta() {
        homePageObjectTask3 = new task3.HomePageObject();
        homePageObjectTask3.goToDelta();
        homePageObjectTask3.booking();

        ticketsSelectionPageObject = new task3.TicketsSelectionPageObject();
        ticketsSelectionPageObject.ticketsChoice();

        summaryPageObject = new task3.SummaryPageObject();
        summaryPageObject.clickContinue();

        passengerInfoPageObject = new task3.PassengerInfoPageObject();
        passengerInfoPageObject.fillFields();

        reviewPage = new task3.ReviewPage();
        assertTrue(reviewPage.finish());
    }

    @Test(priority = 3)
    public void connectToFTP() throws IOException {
        ftpServer = new FTPServer();
        ftpServer.connection();
    }

    @Test(priority = 4)
    public void findTrump() throws IOException {
        CnnReader.countTrumps("http://edition.cnn.com/");
    }
}
